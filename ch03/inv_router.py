from sqlalchemy import create_engine, Column, Integer, String, Float
from sqlalchemy.orm import sessionmaker, declarative_base
from fastapi import APIRouter, HTTPException, status
from pydantic import BaseModel


inv_router = APIRouter()
# from sqlalchemy.ext.declarative import declarative_base

# 1. create_engine
# 2. session
# 3. generate table using declarative_base from model
# 4. execute - CRUD

db_url = "sqlite:///./inventory.db"
engine = create_engine(db_url, connect_args={"check_same_thread":False})

Session = sessionmaker(bind=engine)
db = Session()

# get the parent of all models
# migration - coding that generate table
Base = declarative_base()
class Item(Base):
    __tablename__ = 'item'
    id = Column(Integer, primary_key=True) # primary_key = auto increment
    name = Column(String(100))
    price = Column(Float)

class Item2(BaseModel):
    name: str
    price: float

# moment where a table is generated
Base.metadata.create_all(engine)

# insert a data
# item = Item(name='Shoes', price=9.99)
# db.add(item) # insert into item (name, price) values('Shoes', 9.99)

# list all the inventory
@inv_router.get('/inv')
def get_all():
    # query data
    rows = db.query(Item).all() # select * from item
    db.close()
    return rows
    # for row in rows:
    #     print(row.id, row.name, row.price)
    
    # commit  - insert / update / delete
    # db.commit()

# get one records of item
@inv_router.get('/inv/{id}')
def get_one(id: int):
    item = db.query(Item).filter(Item.id == id).first()
    db.close()
    return item


# insert new item
# pydantic features: 1. validation of data, 2. auto convert request body to an object
@inv_router.post('/inv')
def create(item2:Item2):
    item = Item(name=item2.name, price=item2.price)
    db.add(item)
    db.commit()
    db.refresh(item) # get the latest data after insert
    db.close()
    return item


# update an item
@inv_router.put('/inv/{id}')
def update(item2: Item2, id:int):
    item = db.query(Item).filter(Item.id == id).first()
    item.name = item2.name
    item.price = item2.price
    db.commit()
    db.refresh(item)
    db.close()
    return item

# delete an item
@inv_router.delete('/inv/{id}')
def delete(id:int):
    item = db.query(Item).filter(Item.id == id).first()

    if not item:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Id does not exist")
    
    db.delete(item)
    db.commit()
    db.close()
    return {'message': 'Record has been deleted'}