from fastapi import APIRouter, HTTPException, status
from model import TodoItems

todo_router = APIRouter()
todo_list = [
    {"id": 1, "item":"testing 1"},
    {"id": 2, "item":"testing 2"}
] # this is ok
# todo_list = [1,2,3,4}] # this is not ok

@todo_router.get('/todo/{to_id}', status_code=201)
async def get_one(todo_id: int) ->dict:
    for todo in todo_list:
        if todo['id'] == todo_id:
            return {'todo': todo}

    raise HTTPException(
        status_code = status.HTTP_404_NOT_FOUND,
        detail = "Todo with supplied ID does not exist"
    )

# return all todos
@todo_router.get('/todo', response_model=TodoItems)
async def retrieve_todo() -> dict:
    return {
        "todos": todo_list
    }


# async - concurrent request
# -> dict - return format of this func


