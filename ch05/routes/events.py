from fastapi import APIRouter, Body, HTTPException, status
from models.events import Event
from typing import List

event_router = APIRouter()

events = [] # all events will be registered here

# list all events
@event_router.get('/', response_model=List[Event])
async def retrieve_all_events() -> List[Event]:
    return events

# get one events
@event_router.get('/{id}', response_model=Event)
async def retrieve_event(id: int) -> Event:
    for event in events:
        if event.id == id:
            return event

    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail='ID does not exist'
    )

# create new event
@event_router.post('/new')
async def create_event(body: Event = Body(...)) -> dict:
    events.append(body)
    return {
        'message': 'Event created successfully'
    }

# delete an event /event/4
@event_router.delete('/{id}')
async def delete_event(id: int) -> dict:
    for event in events:
        if event.id == id:
            events.remove(event)
            return {
                'message': 'Event deleted successfully'
            }
        
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail='ID Does not exist'
    )