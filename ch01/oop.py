print("my name is azman".upper())

# class and obj + inheritance
# blueprint vs actual obj

class House:
    # method
    def printMe(self):
        print('this is a house')

# room extends House
class Room(House):
    def printMe2(self):
        print('this is a room')

house = House()
house.printMe()

room = Room()
room.printMe2()
room.printMe() # this is inherited from House
# ok