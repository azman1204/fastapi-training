from fastapi import FastAPI

main = FastAPI()

# @ -> decorator\
# main -> instance name of FastAPI
# get() -> method name, get, post, put, patch, delete
# / -> path of URL
# / = http://localhost:8000/
# /list = http://localhost:8000/list

# decorator
# a node
@main.get('/')
def welcome():
    return "Hello World"

@main.get('/list')
def list():
    # convert dict to JSON
    return {'name': 'Azman'}

