# collection
# 1. list
# 2. dictionary
# 3. tuple
# 4. set

# list
names = ['ali', 'john', 'ravi']
print(names[1]) # john

# dictionary
person = {'name':'John Doe', 'address':'London'}
print(person['name'], person['address'])

# tuple - immutable
person2 = ('Jane Doe', 21)
print(person2[0])

# set - unique
fruits = {'apple', 'pear', 'pisang', 'pear'}
print(fruits)

for fruit in fruits:
    print(fruit)