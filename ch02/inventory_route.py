from fastapi import APIRouter
from pydantic import BaseModel
import sqlite3

inv_router = APIRouter()
path = 'C:\\Users\\azman\\Desktop\\fastapi training\\training\\ch02\\inventory.db'
conn = sqlite3.connect(path, check_same_thread=False)
cur = conn.cursor()

class Item(BaseModel):
    name:str
    price: float

# list all inventory
@inv_router.get('/inv')
def get_all():
    sql = "SELECT * FROM items"
    items = list(cur.execute(sql))
    return items


# get one item from inv
@inv_router.get('/inv/{id}')
def get_one(id:int):
    sql = "SELECT * FROM items WHERE id = " + str(id)
    item = list(cur.execute(sql))
    return item


# create new inventory
@inv_router.post('/inv')
def create(item: Item):
    sql = "INSERT INTO items(name, price) VALUES(?, ?)"
    cur.execute(sql, [item.name, item.price])
    return {'message': 'ok'}


# update an inventory
@inv_router.put('/inv/{id}')
def update(item: Item, id:int):
    sql = "UPDATE items SET name = ?, price = ? WHERE id = ?"
    cur.execute(sql, [item.name, item.price, id])
    return {'message': 'ok'}


# delete an inv
@inv_router.delete('/inv/{id}')
def delete(id: int):
    sql = f"DELETE FROM items WHERE id = {id}"
    cur.execute(sql)
    return {'message': 'ok'}