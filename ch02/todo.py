from fastapi import APIRouter
from pydantic import BaseModel

router = APIRouter()
todo_list = []

class Todo(BaseModel):
    id: int
    item: str

class TodoItem(BaseModel):
    item: str

# insert a todo string into list
# google.com?name=john&age=40
@router.post('/todo')
def add_todo(todo: Todo):
    todo_list.append(todo)
    return {'message': 'Todo added successfully'}


@router.post('/todo2')
# todo - query string
def add_todo(todo: str):
    todo_list.append(todo)
    return {'message': 'Todo added successfully'}


# to list all the todos
@router.get('/todo')
def retrieve_todo():
    return {'todos': todo_list}


@router.get('/hello')
def say_hello():
    return {'message': 'Hello!'}

# get one todo
@router.get('/todo/{todo_id}')
def get_single_todo(todo_id: int):
    for todo in todo_list:
        if todo.id == todo_id:
            return {"todo": todo}
    
    return {'message': "To do with supplied id doesn't exist"}


# update todo item
@router.put('/todo/{todo_id}')
def update_todo(todo_data: TodoItem, todo_id: int):
    for todo in todo_list:
        if todo.id == todo_id:
            todo.item = todo_data.item # data coming in
            return {'message': 'Todo updated successfully'}

    return {'message': 'Todo with supplied id doesnt exist'}


@router.delete('/todo/{todo_id}')
def delete_single_todo(todo_id:int):
    index = 0
    for todo in todo_list:
        todo =  todo_list[index]

        if todo.id == todo_id:
            todo_list.pop(index)
            return {'message': 'to do deleted successfully'}
        
        index += 1

    return {'message': 'Todo with supplied id doesnt exist'}