from fastapi import FastAPI
from todo import router
from inventory_route import inv_router

main = FastAPI()
main.include_router(router)
main.include_router(inv_router)