from fastapi import APIRouter
from models.events import Event, EventUpdate
from database.connection import Database

event_database = Database(Event)

event_router = APIRouter()

@event_router.post('/new')
async def create_event(body: Event) -> dict:
    await event_database.save(body)
    return {
        'message': 'Event succesfully created'
    }

@event_router.get('/')
async def get_all():
    events = await event_database.get_all()
    return events

@event_router.get('/{id}')
async def get_one(id:str):
    event = await event_database.get_one(id)
    return event

@event_router.delete('/{id}')
async def delete(id:str):
    await event_database.delete(id)
    return {
        'message': 'Data has been deleted'
    }


@event_router.put('/{id}')
async def update(id:str, body:EventUpdate):
    await event_database.update(id)
    return {
        'message': 'Data has been updated'
    }