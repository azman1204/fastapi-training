from beanie import init_beanie
from motor.motor_asyncio import AsyncIOMotorClient
from typing import Optional
from pydantic import BaseSettings
from models.events import Event

class Settings(BaseSettings):
    DATABASE_URL: Optional[str] = None
    
    async def initialize_database(self):
        client = AsyncIOMotorClient(self.DATABASE_URL)
        await init_beanie(database=client.get_default_database(), document_models=[Event])

    class Config:
        env_file = ".env" # all settings will be stored in this file


class Database:
    # constructor
    def __init__(self, model):
        self.model = model

    # insert into...
    async def save(self, document) -> None:
        await document.create()
        return
    
    async def get_all(self):
        docs = await self.model.find_all().to_list()
        return docs

    async def get_one(self, id):
        doc = await self.model.get(id)
        return doc
    
    async def delete(self, id):
        doc = await self.model.get(id)
        await doc.delete()
        return True
    
    async def update(self, id):
        doc = await self.model.get(id)
        print(doc)
        # doc.title = 'testing...'
        await doc.update({"$set":{"title":"testing"}})
        return True