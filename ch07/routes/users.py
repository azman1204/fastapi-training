from fastapi import APIRouter, HTTPException, status, Depends
from models.users import User, TokenResponse
from auth.hash_password import HashPassword
from database.connection import Database
from fastapi.security import OAuth2PasswordRequestForm
from auth.jwt_handler import create_access_token

user_router = APIRouter()
hash_password = HashPassword()
user_database = Database(User)

# user login / authentication
@user_router.post('/signin', response_model=TokenResponse)
async def sign_in_user(user: OAuth2PasswordRequestForm = Depends()) -> dict:
    # query db, get the user
    user_exist = await User.find_one(User.email == user.username)

    # checking username
    if not user_exist:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail='User does not exist'
        )
    
    # chekc password
    if hash_password.verify_hash(user.password, user_exist.password):
        access_token = create_access_token(user_exist.email)
        return {
            "access_token": access_token,
            "token_type": "Bearer"
        }
    
    # password incorrect
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail='Invalid details passsed'
    )


# user registration / signup
@user_router.post('/signup')
async def sign_user_up(user: User) -> dict:
    # find_one - see beanie docu
    user_exist = await User.find_one(User.email == user.email)
    if user_exist:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail='User with the email already exist'
        )
    
    # save data to mongo DB
    hashed_password = hash_password.create_hash(user.password)
    user.password = hashed_password
    await user_database.save(user)
    return {
        'message': 'User created successfully'
    }


