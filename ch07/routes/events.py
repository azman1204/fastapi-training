from fastapi import APIRouter, Depends, HTTPException, status
from auth.authenticate import authenticate
from models.events import Event, EventUpdate
from database.connection import Database
from beanie import PydanticObjectId

event_router = APIRouter()
event_database = Database(Event)

@event_router.post('/new')
# user = email
async def create_event(body: Event, user: str = Depends(authenticate)):
    body.creator = user
    await event_database.save(body)
    return {
        'message' : 'Event created successfully'
    }


@event_router.put("/{id}")
async def update_event(id: PydanticObjectId, body:EventUpdate, user:str = Depends(authenticate)): 
    event = await event_database.get_one(id)
    
    # creator is the one who can update the event
    if event.creator != user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Operation not allowed'
        )
    
    updated_event = await event_database.update(id, body)
    print(update_event)
    if not updated_event:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail='Cannot update data'
        )
    
    return updated_event


@event_router.delete('/{id}')
async def delete(id:PydanticObjectId, user: str = Depends(authenticate)):
    event = await event_database.get_one(id)
    print(event)
    # creator is the one who can delete the event
    if event.creator != user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Operation not allowed'
        )
    
    await event_database.delete(id)
    return {
        'message': 'Record has been deleted'
    }