from pydantic import BaseModel, EmailStr
from typing import List, Optional
from models.events import Event
from beanie import Document

# this to register a user
class User(Document):
    email: EmailStr
    password: str
    username: str
    events: Optional[List[Event]] =  None

    # this show which collection this model represent
    class Setting:
        name = "users"

# this class will be used to log-in into app
class UserSignIn(BaseModel):
    email: EmailStr
    password: str


class TokenResponse(BaseModel):
    access_token: str
    token_type: str