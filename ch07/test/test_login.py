import httpx
import pytest

@pytest.mark.asyncio
async def test_sign_new_user(default_client: httpx.AsyncClient)->None:
    payload = {
        "email": "test2@yahoo.com",
        "password": "12345",
        "username": "John Doe"
    }

    headers = {
        "accept": "application/json",
        "Content-Type": "application/json"
    }

    # this is the kind of response that we expect
    test_response = {
        "message": "User created successfully"
    }

    response = await default_client.post('/user/signup', json=payload, headers=headers)
    assert response.json() == test_response