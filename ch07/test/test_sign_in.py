import pytest
import httpx

@pytest.mark.asyncio
async def test_sign_in_user(default_client: httpx.AsyncClient) -> None:
    payload = {
        "username": "test@yahoo.com",
        "password": "12345"
    }

    headers = {
        "accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
    }

    response = await default_client.post("/user/signin", data=payload, headers=headers)
    assert response.status_code == 200
    assert response.json()['token_type'] == "Bearer"
    # print(response.json()['access_token'])