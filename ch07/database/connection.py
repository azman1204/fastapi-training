from beanie import init_beanie
from motor.motor_asyncio import AsyncIOMotorClient
from typing import Optional, Any
from pydantic import BaseSettings, BaseModel
from models.events import Event
from models.users import User
from beanie import PydanticObjectId

class Settings(BaseSettings):
    DATABASE_URL: Optional[str] = None
    SECRET_KEY: Optional[str] = None
    
    async def initialize_database(self):
        client = AsyncIOMotorClient(self.DATABASE_URL)
        await init_beanie(database=client.get_default_database(), document_models=[Event, User])

    class Config:
        env_file = ".env.prod" # all settings will be stored in this file


class Database:
    # constructor
    def __init__(self, model):
        self.model = model

    # insert into...
    async def save(self, document) -> None:
        await document.create()
        return
    
    async def get_all(self):
        docs = await self.model.find_all().to_list()
        return docs

    async def get_one(self, id):
        doc = await self.model.get(id)
        # print(doc)
        return doc
    
    async def delete(self, id):
        doc = await self.model.get(id)
        # print(doc)
        await doc.delete()
        return True
    
    async def update(self, id: PydanticObjectId, body: BaseModel) -> Any:
        doc_id = id
        des_body = body.dict()

        des_body = {k: v for k, v in des_body.items() if v is not None}
        update_query = {"$set": {
            field: value for field, value in des_body.items()
        }}

        doc = await self.get_one(doc_id)
        if not doc:
            return False
        
        print(update_query)
        await doc.update(update_query)
        return doc