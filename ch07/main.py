from fastapi import FastAPI
from routes.users import user_router
from routes.events import event_router
# from routes.events import event_router
import uvicorn
from database.connection import Settings

app = FastAPI()
# prefix means all routes will start with /user. i.e http://.../user/signup
app.include_router(user_router, prefix='/user')
app.include_router(event_router, prefix='/event')
# app.include_router(event_router, prefix='/event')

# this will open connection to DB and create tables
# automatically run
setting = Settings()

@app.on_event("startup")
async def on_startup():
    await setting.initialize_database()


# if you run this directly, then run the uvicorn
if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8082, reload=True)