# encode and decode JWT string
# encode - generate the JWT string  (head, payload, signature)
# decode - read the payload from the JWT string
import time
from database.connection import Settings
from jose import jwt, JWTError
from fastapi import HTTPException, status
from datetime import datetime

settings = Settings()

def create_access_token(user: str) -> str:
    payload = {
        "user": user,
        "expires": time.time() + 3600 # 1 hour = 3600 seconds
    }
    token = jwt.encode(payload, settings.SECRET_KEY, algorithm="HS256")
    return token

def verify_access_token(token:str) -> dict:
    try:
        # data - is a dict of payload (user, expires) {'user': ..., 'expires':..}
        data = jwt.decode(token, settings.SECRET_KEY, algorithms="HS256")
        expire = data.get('expires')

        # token empty, or token not in correct format
        if expire is None:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='No token supplied'
            )
        
        # this token expired already
        if datetime.utcnow() > datetime.utcfromtimestamp(expire):
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail='Token expired!'
            )
        
        return data
    except JWTError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Invalid Token'
        )