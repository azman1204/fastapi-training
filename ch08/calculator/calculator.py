def add(a:int, b:int)->int:
    return a + b

def substract(a:int, b:int)->int:
    return b - a

def multiply(a:int, b:int)->int:
    return b * a

# /  - return float
# // - return int
def divide(a:int, b:int)->int:
    return b // a