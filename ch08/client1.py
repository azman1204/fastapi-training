import requests

response = requests.get('http://localhost:8085/event')
events = response.json()
for event in events:
    print(event['title'])


payload = {
    "title": "abc", 
    "image":"abc.png", 
    "description": "abc desc", 
    "tags": ["1"], 
    "location": "Bangsar"
}

res = requests.post('http://localhost:8085/event/new', json=payload)
print(res.json())
