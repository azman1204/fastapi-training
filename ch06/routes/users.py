from fastapi import APIRouter, HTTPException, status
from models.users import User, UserSignIn

user_router = APIRouter()

users = {}

@user_router.post('/signup')
async def sign_new_user(data: User) -> dict:
    if data.email in users:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail='User with supplied username is already exist'
        )
    
    users[data.email] = data
    return {'message': 'User successfully registered'}


@user_router.post('/signin')
async def sign_user_in(user: UserSignIn) ->dict:
    # checking email exist in dict
    if user.email not in users:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail='Wrong credential passed'
        )
    
    # compare submitted password
    if users[user.email].password != user.password:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail='Wrong credential passed'
        )
    
    return {
        'message': 'User signed in successfully'
    }