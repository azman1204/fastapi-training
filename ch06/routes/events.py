from fastapi import APIRouter, Body, HTTPException, status, Depends
from models.events import Event, EventUpdate
from typing import List
from database.connection import get_session
from sqlmodel import select

event_router = APIRouter()

events = [] # all events will be registered here

# list all events
@event_router.get('/', response_model=List[Event])
async def retrieve_all_events(session=Depends(get_session)) -> List[Event]:
    statement = select(Event)
    events = session.exec(statement).all()
    return events

# get one events
@event_router.get('/{id}', response_model=Event)
async def retrieve_event(id: int, session=Depends(get_session)) -> Event:
    event = session.get(Event, id) # select * from event where id = ?
    if event:
        return event

    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail='ID does not exist'
    )

# create new event
# Depends - used to do dependency injection
@event_router.post('/new')
async def create_event(new_event: Event, session=Depends(get_session)) -> dict:
    session.add(new_event)
    session.commit()
    session.refresh(new_event)
    return {
        'message': 'Event created successfully'
    }

# delete an event /event/4
@event_router.delete('/{id}')
async def delete_event(id: int, session=Depends(get_session)) -> dict:
    event = session.get(Event, id)
    if event:
        session.delete(event)
        session.commit()
        return {
            'messasge': 'Record deleted successfully'
        }
    
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail='ID Does not exist'
    )


# update a record
@event_router.put('/{id}')
async def update_event(id: int, new_data:EventUpdate, session=Depends(get_session)) -> dict:
    event = session.get(Event, id)
    if event:
        event_data = new_data.dict(exclude_unset=True)
        for key, value in event_data.items():
            setattr(event, key, value)

        # event.description = new_data.description

        session.add(event)
        session.commit()
        session.refresh(event)
        return event
    
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail='ID Does not exist'
    )