from fastapi import FastAPI
from routes.users import user_router
from routes.events import event_router
import uvicorn
from database.connection import conn
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()
# prefix means all routes will start with /user. i.e http://.../user/signup
app.include_router(user_router, prefix='/user')
app.include_router(event_router, prefix='/event')

# see pg 167
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*']
)

# this will open connection to DB and create tables
# automatically run
@app.on_event("startup")
def on_startup():
    conn()

# if you run this directly, then run the uvicorn
if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8085, reload=True)