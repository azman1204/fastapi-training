from sqlmodel import create_engine, SQLModel, Session
from models.events import Event

database_file = "database.db"
database_connection_string = f'sqlite:///{database_file}'
connection_args = {"check_same_thread": False}
engine = create_engine(database_connection_string, echo=True, connect_args=connection_args)

def conn():
    # moment where all tables are created
    SQLModel.metadata.create_all(engine)

def get_session():
    with Session(engine) as session:
        yield session