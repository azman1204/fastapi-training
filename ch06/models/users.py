from pydantic import BaseModel, EmailStr
from typing import List, Optional
from models.events import Event

# this to register a user
class User(BaseModel):
    email: EmailStr
    password: str
    username: str
    events: Optional[List[Event]] =  None


# this class will be used to log-in into app
class UserSignIn(BaseModel):
    email: EmailStr
    password: str